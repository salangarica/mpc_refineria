import pandas as pd
import numpy as np
import os
import glob
import matplotlib
import matplotlib.pyplot as plt
import time
#from time import time
from datetime import datetime,timedelta
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import random
#import seaborn as sns
from sklearn.metrics import mean_squared_error
import tensorflow as tf
from pyswarms.backend import operators
from pyswarms.single.global_best import GlobalBestPSO
import logging


#### Arquitectura de RNN LSTM ####

# Cosecha
def RNN_LSTM_Cosecha(n_outputs):
    # Parametros
    n_neurons = 50
    n_inputs = 30
    n_layers = 2
    learning_rate = 0.001
    # Arquitectura
    tf.reset_default_graph()
    x = tf.placeholder(tf.float32, [None, None, n_inputs])
    y = tf.placeholder(tf.float32, [None, None, n_outputs])
    cell = tf.contrib.rnn.MultiRNNCell([tf.contrib.rnn.LSTMCell(n_neurons) for i in range(n_layers)])
    cell_wrapped = tf.contrib.rnn.OutputProjectionWrapper(cell, output_size=n_outputs)
    outputs, states = tf.nn.dynamic_rnn(cell_wrapped, x, dtype=tf.float32)
    loss = tf.reduce_mean(tf.square(outputs - y))
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    training_op = optimizer.minimize(loss)
    # Inicialización
    init = tf.global_variables_initializer()
    net = tf.Session()
    net.run(init)
    saver = tf.train.Saver()

    return x, y, net, outputs, training_op, saver


# Electrolito
def RNN_LSTM_Circ():
    # Parametros
    n_neurons = 50
    n_inputs = 24
    n_outputs = 3
    batch_size = 20
    n_layers = 2
    learning_rate = 0.001
    horizonte = 1
    # Arquitectura
    tf.reset_default_graph()
    x = tf.placeholder(tf.float32, [None, batch_size, n_inputs])
    y = tf.placeholder(tf.float32, [None, batch_size, n_outputs])
    cell = tf.contrib.rnn.MultiRNNCell([tf.contrib.rnn.LSTMCell(n_neurons) for i in range(n_layers)])
    cell_wrapped = tf.contrib.rnn.OutputProjectionWrapper(cell, output_size=n_outputs)
    outputs, states = tf.nn.dynamic_rnn(cell_wrapped, x, dtype=tf.float32)
    loss = tf.reduce_mean(tf.square(outputs - y))
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    training_op = optimizer.minimize(loss)
    # Inicialización
    init = tf.global_variables_initializer()
    net = tf.Session()
    net.run(init)
    saver = tf.train.Saver()

    return x, y, net, outputs, training_op, saver


def Run_models(Vector_Seccion_Activa, Data_Circuito, fecha_iteracion, numero_ch, Data_Seccion, net, outputs, x,
               net_eficiencia, outputs_eficiencia, xef, net_comp_quim, outputs_comp_quim, xcq, net_rechazo,
               outputs_rechazo, xrech, Max_min_output_cosecha, Max_min_output_circuito, Max_min_input_cosecha,
               Max_min_input_circuito):
    i = numero_ch

    Variables_predecir_electrolito = ['el_as', 'el_bi', 'el_sb']
    Secciones_CH = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

    # Calcular el batch de tiempo para el modelo del Electrolito
    fecha_inicio_prediccion_ch = fecha_iteracion - timedelta(20)



    # Condiciones de fecha para Formar el DF para el modelo de electrolito
    condicion_fecha_prediccion_ch_1 = pd.to_datetime(Data_Circuito.loc[i]['fecha'].str.split(' ', expand=True)[0],
                                                     yearfirst=True, utc=True) <= fecha_iteracion
    condicion_fecha_prediccion_ch_2 = pd.to_datetime(Data_Circuito.loc[i]['fecha'].str.split(' ', expand=True)[0],
                                                     yearfirst=True, utc=True) >= fecha_inicio_prediccion_ch
    condicion_fecha_prediccion_ch = condicion_fecha_prediccion_ch_1.values * condicion_fecha_prediccion_ch_2.values

    DF_INPUT_CH = Data_Circuito.loc[i][condicion_fecha_prediccion_ch].drop('fecha', axis=1)

    # Normalizar a MinMax y Predecir
    prediccion_ch = net.run(outputs, feed_dict={
        x: Max_min_input_circuito.transform(DF_INPUT_CH).reshape([-1, DF_INPUT_CH.shape[0], DF_INPUT_CH.shape[1]])})
    prediccion_ch = prediccion_ch.reshape([prediccion_ch.shape[1], prediccion_ch.shape[2]])[-1, :]

    # DF de la predicción de electrolito (escalada)
    PREDICCION_CH = pd.DataFrame(prediccion_ch, index=Variables_predecir_electrolito, columns=[i]).T

    # En base a fecha de iteracion, se obtiene las cosechas activas
    condicion_secciones_activas = pd.to_datetime(Data_Seccion.loc[i]['fecha'],
                                                 yearfirst=True, utc=True) == fecha_iteracion
    #print(fecha_iteracion)

    #print(pd.to_datetime(Data_Seccion.loc[i]['fecha'].str.split(' ', expand=True)[0],
    #                                             yearfirst=True, utc=True))




    # Obteno los datos de las cosechas activas
    DF_Secciones_Activas = Data_Seccion.loc[i][condicion_secciones_activas]

    # Eliminar cosechas segun Vector_Booleando de Entrada (Modificable fuera de la función)
    Secciones_Activas_Dia = DF_Secciones_Activas['seccion'].values
    Secciones_A_Predecir = np.intersect1d(Secciones_CH[Vector_Seccion_Activa], Secciones_Activas_Dia)

    DIAS = []  # Acumular los dias que quedan
    Acumulador = []  # Acumulador de DF
    for j in Secciones_A_Predecir:  # Iterar por secciones validas

        # Obtener los datos por seccion
        Seccion_A_Evaluar = DF_Secciones_Activas[DF_Secciones_Activas['seccion'] == j]

        # fecha inicio
        fecha_energizacion = Seccion_A_Evaluar['fecha_energizacion'].str.split(' ', expand=True)[0].iloc[0]

        # ID de seccion
        id_seccion = Seccion_A_Evaluar['id']

        # Condicion de fecha para obtener los datos a nivel de circuito
        condicion_fecha_ch_sec_1 = pd.to_datetime(Data_Circuito.loc[i]['fecha'],
                                                  yearfirst=True, utc=True) >= pd.to_datetime(fecha_energizacion,
                                                                                              dayfirst=True, utc=True)
        condicion_fecha_ch_sec_2 = pd.to_datetime(Data_Circuito.loc[i]['fecha'],
                                                  yearfirst=True, utc=True) <= fecha_iteracion
        condicion_fecha_ch_sec = condicion_fecha_ch_sec_1.values * condicion_fecha_ch_sec_2.values

        DF_CH_SEC = Data_Circuito.loc[i][condicion_fecha_ch_sec]
        DF_CH_SEC = DF_CH_SEC.sort_values(by=['fecha'])


        # Obtendo los datos con la cosecha actual (filtro las cosechas que ya ocurrieron
        DF_SECCION_V1 = Data_Seccion.loc[i][Data_Seccion.loc[i]['seccion'] == j].copy()

        DF_SECCION_V1 = DF_SECCION_V1[
            pd.to_datetime(DF_SECCION_V1['fecha_energizacion'].str.split(' ', expand=True)[0], yearfirst=True,
                           utc=True) == fecha_energizacion]


        DF_SECCION_V1 = DF_SECCION_V1[
            pd.to_datetime(DF_SECCION_V1['fecha'], yearfirst=True,
                           utc=True) <= fecha_iteracion]




        DF_SECCION_V1 = DF_SECCION_V1[:-1]
        # print ('print DF_SECCION')
        # print(DF_SECCION_V1)
        DIAS.append(DF_SECCION_V1['dia'].max())  # Duracion actual de la cosecha

        #index_variable_seccion = DF_SECCION_V1.columns[5:]  # Variables
        # print(index_variable_seccion)
        # Actualizar los datos de cosecha con los datos a nivel de circuito

        # print(DF_CH_SEC)
        filtro_variables = np.intersect1d(DF_SECCION_V1.columns[5:], DF_CH_SEC.columns[1:])



        DF_SECCION_V1[filtro_variables] = DF_CH_SEC[filtro_variables].values

        # Primer Input con las variables de la cosech

        DF_INPUT_SEC_1 = DF_SECCION_V1[DF_SECCION_V1.columns[5:]]
        DF_INPUT_SEC_1 = DF_INPUT_SEC_1.reset_index(drop=True)
        # Normalizar a MinMax
        DF_INPUT_SEC_1 = pd.DataFrame(Max_min_input_cosecha.transform(DF_INPUT_SEC_1), index=DF_INPUT_SEC_1.index,
                                      columns=DF_INPUT_SEC_1.columns)

        # Segundo Input con las predicciones de electrolito
        DF_INPUT_SEC_2 = pd.concat([DF_INPUT_SEC_1[Variables_predecir_electrolito][1:], PREDICCION_CH],
                                   ignore_index=True)
        DF_INPUT_SEC_2.columns = 'Out_' + DF_INPUT_SEC_2.columns

        # Concateno Input 1 e Input 2
        x_test = pd.concat([DF_INPUT_SEC_1, DF_INPUT_SEC_2], axis=1, sort=False)
        x_test = x_test.values.reshape([-1, x_test.shape[0], x_test.shape[1]])

        # Prediccion Eficiencia
        yf = net_eficiencia.run(outputs_eficiencia, feed_dict={xef: x_test})[0, -1, 0]
        # Prediccion Comp Quimicos
        yc = net_comp_quim.run(outputs_comp_quim, feed_dict={xcq: x_test})[0, -1, :]
        # Prediccion Rechazo
        yr = net_rechazo.run(outputs_rechazo, feed_dict={xrech: x_test})[0, -1, 0]

        # DF por SECCION
        PREDICCION_SEC_1 = pd.DataFrame(np.array([yf, yr, yc[0], yc[1], yc[2], yc[3]]), columns=[id_seccion],
                                        index=['Eficiencia', 'Rechazo', 'Ag', 'As', 'S', 'Sb']).T
        # SE GUARDA en una lista
        Acumulador.append(PREDICCION_SEC_1)
    # DF de las prediccion por sección DEL Circuito Hidraulico
    PREDICCION_SECC = pd.concat(Acumulador)

    # PREDICCION_SECC.reindex(index = newindex)
    # Formo DF de Días que lleva activa
    DIAS = pd.DataFrame(DIAS, index=PREDICCION_SECC.index, columns=['Dias Activa'])
    # Reescalo la prediccion del electrolito
    PREDICCION_CH = pd.DataFrame(Max_min_output_circuito.inverse_transform(PREDICCION_CH), index=PREDICCION_CH.index,
                                 columns=PREDICCION_CH.columns)
    # Reescalo la predicción por cosecha/seccion
    PREDICCION_SECC = pd.DataFrame(Max_min_output_cosecha.inverse_transform(PREDICCION_SECC),
                                   index=PREDICCION_SECC.index, columns=PREDICCION_SECC.columns)


    return DIAS, PREDICCION_CH, PREDICCION_SECC


def string_to_date(date_string, str_format='%Y-%m-%d %H:%M:%S'):
    return datetime.strptime(date_string, str_format)


def date_to_string(date, str_format='%Y-%m-%d %H:%M:%S'):
    return date.strftime(str_format)



def extractDigit(number, place):
    temp = number / place
    value=int(round(temp % 10))
    if value==0:
        value=value+10
    return value

def change_vector_seccion_activa(Vector_Seccion_Activa, DIAS, n):
    # transformo index a index de 1 a 10
    newindex = [0] * len(DIAS)

    for j in (range(len(DIAS))):
        newindex[j] = extractDigit(DIAS.index[j][0], 100)

    DIAS = DIAS.reset_index(drop=True)
    DIAS['new_col'] = newindex
    DIAS.set_index('new_col')
    new_index = np.arange(1, 11, 1)
    DIAS = DIAS.set_index('new_col').reindex(new_index, fill_value='NaN')
    # print(DIAS)
    # print(Vector_Seccion_Activa)
    for k in range(len(DIAS)):
        DIAS['Dias Activa'].values
        if ((float(DIAS['Dias Activa'][k + 1]) >= n) or DIAS['Dias Activa'][k + 1] == 'NaN'):
            Vector_Seccion_Activa[k] = False

    return Vector_Seccion_Activa



class OptimalSolution:

    def __init__(self, prediction_horizon,
                 control_horizon, n, m,
                 effective_iters=0, opt_start=datetime.utcnow()):

        self.N_y = prediction_horizon
        self.N_u = control_horizon
        self.trajectory = np.zeros((self.N_y, n), float)
        self.sequence = np.zeros((self.N_u, m), float)
        self.incremental_sequence = np.zeros((self.N_u, m), float)
        self.effective_iters = effective_iters
        self.optimization_instant = opt_start
        # Output of PSO for checking
        self.pso_output_names = ['a_compute_time', 'b_ufr_optimal', 'c_ffr_optimal',
                                 'd_pressure_optimal', 'e_usc_optimal', 'f_bed_optimal', 'g_torque_optimal',
                                 'h_total_cost', 'i_exit_flag', 'j_mean_iter_time',
                                 'k_inc_ufr_opt', 'l_inc_ffr_opt']
        self.cost = np.inf
        self.compute_time = np.inf
        self.mean_iter_time = float(0)
        self.exit_flag = 10

    def arrays_to_dframe(self, horizon, sample_time, ts_from):

        tm = ts_from - timedelta(minutes=ts_from.minute % sample_time,
                                 seconds=ts_from.second,
                                 microseconds=ts_from.microsecond)


        times = pd.date_range(tm, periods=horizon,
                              freq=str(sample_time) + 'T')


        computed_time = np.tile(self.compute_time, (horizon, 1))
        mean_iter_time = np.tile(self.mean_iter_time, (horizon, 1))
        exit_flag = np.tile(self.exit_flag, (horizon, 1))
        cost = np.tile(self.cost, (horizon, 1))

        # For incremental encoding
        n_u = self.incremental_sequence.shape[0]/self.sequence.shape[1]
        inc_ufr = np.pad(self.incremental_sequence[::self.sequence.shape[1]],
                         (0, int(horizon-n_u)), 'constant', constant_values=(0, 0))
        inc_ffr = np.pad(self.incremental_sequence[1::self.sequence.shape[1]]/100,
                         (0, int(horizon - n_u)), 'constant', constant_values=(0, 0))
        inc_ufr = inc_ufr.reshape((horizon, 1))
        inc_ffr = inc_ffr.reshape((horizon, 1))
        inc_mv = np.hstack((inc_ufr, inc_ffr))

        column_values = np.hstack((computed_time, self.sequence,
                                   self.trajectory, cost, exit_flag, mean_iter_time,
                                   inc_mv))

        df_out = pd.DataFrame(column_values,
                              index=times,
                              columns=self.pso_output_names)

        return df_out


class MyOptimizer(GlobalBestPSO):
    def optimize(self, objective_func, iter_limits, print_step=1, verbose=1, options=None, **kwargs):
        """Optimize the swarm for a number of iterations

                Performs the optimization to evaluate the objective
                function :code:`f` for a number of iterations :code:`iter.`

                Parameters
                ----------
                objective_func : function
                    objective function to be evaluated
                iter_limits : list
                    number of iterations, stall iters, particle_iters
                print_step : int (default is 1)
                    amount of steps for printing into console.
                verbose : int  (default is 1)
                    verbosity setting.
                kwargs : dict
                    arguments for the objective function
                options : dict
                    arguments for plotting, matlab PSO, etc.

                Returns
                -------
                tuple
                    the global best cost and the global best position.
                """
        # Initialize PSO options
        exec_time = 0
        iters = iter_limits[0]
        max_stall_iters = iter_limits[1]
        champion_distance_iters = iter_limits[2]

        stall_iters_break = False
        champion_iters_break = False
        timeout = False
        stall_iters_counter = 0
        champion_stall_counter = 0

        # Swarm initialization cost
        self.swarm.pbest_cost = np.full(self.swarm_size[0], np.inf)
        # Compute cost for current position and personal best
        _print_and_log(PRINT_MF, 'i', 'Initializing PSO and pbest_cost.')
        # self.swarm.pbest_cost = objective_func(self.swarm.position, **kwargs)
        # Optional extra arguments
        plot_iters = False
        plot_to = 0
        plot_surface = False
        results_path = ''
        try:
            plot_bool = options['plot']
            if plot_bool:
                plot_iters = options['plot_iters']
                plot_to = options['plot_to']
                plot_surface = options['plot_surface']
        except KeyError:
            plot_bool = False
            pass
        try:
            results_path = options['results_path']+'pso_graphs/'
            save_plots = True
        except KeyError:
            save_plots = False
            pass
        for i in range(iters):
            start_iter_time = time.time()
            lower_cost = True
            timeout = False
            if exec_time > options['timeout']:
                timeout = True
                break
            # Compute cost for current position and personal best
            self.swarm.current_cost = objective_func(self.swarm.position, **kwargs)
            self.swarm.pbest_pos, self.swarm.pbest_cost = operators.compute_pbest(
                self.swarm
            )
            best_cost_yet_found = self.swarm.best_cost
            # Get minima of pbest and check if it's less than gbest
            if np.min(self.swarm.pbest_cost) < self.swarm.best_cost:
                self.swarm.best_pos, self.swarm.best_cost = self.top.compute_gbest(
                    self.swarm
                )
            # Print to console
            if i % print_step == 0:
                _print_and_log(PRINT_MF, 'i', 'Iteration {}/{}, cost: {}', i + 1,
                               iters,
                               self.swarm.best_cost)
            # Save to history
            hist = self.ToHistory(
                best_cost=self.swarm.best_cost,
                mean_pbest_cost=np.mean(self.swarm.pbest_cost),
                mean_neighbor_cost=self.swarm.best_cost,
                position=self.swarm.position,
                velocity=self.swarm.velocity,
            )
            self._populate_history(hist)
            if plot_bool and (i % plot_iters) == 0:
                self.plot_swarm_positions(i, plot_to, plot_surface, save_plots, results_path)
            # Verify stop criteria based on the relative acceptable cost ftol
            # Could change it to whatever my implementation would like
            # Not sure if this is good (making it "relative")
            relative_measure = self.ftol * (1 + np.abs(best_cost_yet_found))
            # relative_measure = self.ftol

            if np.abs(self.swarm.best_cost - best_cost_yet_found) < relative_measure:
                lower_cost = False
            if lower_cost:
                stall_iters_counter = max(0, stall_iters_counter-1)
                if options['mimic_matlab']:
                    if stall_iters_counter < 2:
                        self.swarm.options['w'] = self.swarm.options['w'] * 2
                    if stall_iters_counter > 5:
                        self.swarm.options['w'] = self.swarm.options['w'] / 2
                    self.swarm.options['w'] = min(max(self.swarm.options['w'], options['inertia_range'][0]),
                                                  options['inertia_range'][1])
            else:
                stall_iters_counter = stall_iters_counter+1
            exec_time = exec_time + time.time() - start_iter_time
            # Terminal Conditons
            if stall_iters_counter == max_stall_iters:
                stall_iters_break = True
                break
            # Perform velocity and position updates
            self.swarm.velocity = self.top.compute_velocity(
                self.swarm, self.velocity_clamp
            )
            self.swarm.position = self.compute_position()
        # Obtain the final best_cost and the final best_position
        final_best_cost = self.swarm.best_cost.copy()
        final_best_pos = self.swarm.best_pos.copy()
        # Run out of iterations
        if stall_iters_break:
            exit_flag = 1
        elif timeout:
            exit_flag = -5
        else:
            exit_flag = 0
        _print_and_log(PRINT_MF, 'i', 'Best particle: {}, cost: {}', final_best_pos,
                       final_best_cost)
        return final_best_cost, final_best_pos, exit_flag


    def plot_swarm_positions(self, crt_it, plot_to, plot_surface, save_plots=False, results_path=''):
        fig = plt.figure(figsize=(7, 9))
        plt.ion()
        for tau in range(0, plot_to):
            if plot_surface:
                ax = fig.add_subplot(2, 2, tau+1, projection='3d')
                ax.scatter(self.swarm.position[:, 0 + tau * 2],
                           self.swarm.position[:, 1 + tau * 2],
                           self.swarm.current_cost[:])
                plt.title('Objective surface for t+{}'.format(tau), fontsize=12)
            else:
                ax = fig.add_subplot(2, 2, tau+1)
                ax.scatter(self.swarm.position[:, 0 + tau * 2],
                           self.swarm.position[:, 1 + tau * 2]/100)
                plt.title('Swarm positions t+{}'.format(tau), fontsize=12)
            plt.grid()
            # plt.hold(True)

            # x_surf, y_surf = np.meshgrid(self.swarm.position[:, 0+tau*2], self.swarm.position[:, 1 + tau*2])
            # ax.plot_surface(x_surf, y_surf, self.swarm.current_cost[:], cmap=cm.hot)
            ax.set_xlim(-15, 15)
            ax.set_ylim(-0.1, 0.1)
        plt.draw()
        if save_plots:
            plt.savefig(results_path + 'pstn_{}.svg'.format(crt_it), format='svg')
        else:
            plt.pause(5)
        # input('Press [enter] to continue.')
        # plt.close('all')
        plt.close()
        # plt.clf()

    def compute_position(self):
        """Update the position matrix

        This method updates the position matrix given the current position and
        the velocity. If bounded, it waives updating the position.

        # Parameters
        # ----------
        # swarm : pyswarms.backend.swarms.Swarm
        #     a Swarm instance
        # bounds : tuple of :code:`np.ndarray` or list (default is :code:`None`)
        #     a tuple of size 2 where the first entry is the minimum bound while
        #     the second entry is the maximum bound. Each array must be of shape
        #     :code:`(dimensions,)`.

        Returns
        -------
        numpy.ndarray
            New position-matrix
        """
        swarm = self.swarm
        bounds = self.bounds
        try:
            temp_position = swarm.position.copy()
            temp_position += swarm.velocity

            if bounds is not None:
                lb, ub = bounds
                min_bounds = np.repeat(
                    np.array(lb)[np.newaxis, :], swarm.n_particles, axis=0
                )
                max_bounds = np.repeat(
                    np.array(ub)[np.newaxis, :], swarm.n_particles, axis=0
                )
                # mask = np.all(min_bounds <= temp_position, axis=1) * np.all(
                #     temp_position <= max_bounds, axis=1
                # )

                # My mod to emulate matlab solver
                mask = (min_bounds <= temp_position)
                temp_position = np.where(mask, temp_position, min_bounds)
                swarm.velocity = np.where(mask, swarm.velocity, np.zeros((swarm.n_particles, swarm.dimensions),
                                                                         dtype=float))
                mask = (temp_position <= max_bounds)
                temp_position = np.where(mask, temp_position, max_bounds)
                swarm.velocity = np.where(mask, swarm.velocity, np.zeros((swarm.n_particles, swarm.dimensions),
                                                                         dtype=float))
                # mask = np.repeat(mask[:, np.newaxis], swarm.dimensions, axis=1)
                # temp_position = np.where(~mask, swarm.position, temp_position)
            position = temp_position
        except AttributeError:
            msg = "Please pass a Swarm class. You passed {}".format(type(swarm))
            logger.error(msg)
            raise
        else:
            return position


