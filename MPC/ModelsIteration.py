import pandas as pd
import numpy as np
import os
import glob
import matplotlib
import matplotlib.pyplot as plt
import time
#from time import time
from datetime import datetime,timedelta
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import random
#import seaborn as sns
from sklearn.metrics import mean_squared_error
from models import RNN_LSTM_Cosecha, RNN_LSTM_Circ, extractDigit, Run_models, string_to_date, date_to_string, change_vector_seccion_activa
import tensorflow as tf


################################### Instanciación de los modelos #######################################################
#Directorio modelos
path_load = '../Empaquetado/Modelos/'

#Creamos la RNN Electrolito
x,y,net,outputs,training_op,saver = RNN_LSTM_Circ()
# Cargamos el modelo
saver.restore(net, path_load+'Electrolito_A1.ckpt')


#Creamos la RNN Cosecha
n_outputs = 1 # Eficiencia y Rechazo
n_outputs_cq = 4 #Componentes Químicos


xef,yef,net_eficiencia,outputs_eficiencia,training_op_efi,saver_efi = RNN_LSTM_Cosecha(n_outputs)
xrech,yrech,net_rechazo,outputs_rechazo,training_op_rech,saver_rech = RNN_LSTM_Cosecha(n_outputs)
xcq,ycq,net_comp_quim,outputs_comp_quim,training_op_cq,saver_cq = RNN_LSTM_Cosecha(n_outputs_cq)

# Cargamos los modelos
saver_efi.restore(net_eficiencia, path_load+'Eficiencia_a1.ckpt')
saver_cq.restore(net_rechazo, path_load+'Rechazo_a1.ckpt')
saver_rech.restore(net_comp_quim, path_load+'Comp_Quim_a1.ckpt')


##################################### Carga de Datos ###################################################################
#Cargamos los Datos
my_dir = "../Empaquetado/Datos/"

files = os.listdir(my_dir)
files.sort()
files = [my_dir + file for file in files]


#Cargamos datos del Circuito Hidraulico CH
Data_Circuito = pd.read_csv(files[1], sep=';').sort_values(by='fecha')#.reset_index()

#Cargamos datos de la sección SECC
Data_Seccion = pd.read_csv(files[0], sep=';').sort_values(by='fecha')#.reset_index()




# Valores para el MaxMin Scaler
Max_min_input_cosecha = preprocessing.MinMaxScaler()
Max_min_output_cosecha = preprocessing.MinMaxScaler()
Max_min_input_circuito = preprocessing.MinMaxScaler()
Max_min_output_circuito = preprocessing.MinMaxScaler()


Max_min_input_cosecha.fit_transform(pd.read_csv(files[2], sep=';'))
Max_min_output_cosecha.fit_transform(pd.read_csv(files[3], sep=';'))
Max_min_input_circuito.fit_transform(pd.read_csv(files[4], sep=';'))
Max_min_output_circuito.fit_transform(pd.read_csv(files[5], sep=';'))



# Dejamos como índice los CH para los CH
Data_Circuito = Data_Circuito.set_index('circuito', drop=True)
# Dejamos como índice los CH para la SECC
Data_Seccion = Data_Seccion.set_index('circuito', drop=True)


# Empareja los tags de las mismas variables
Data_Seccion.columns = ['fecha', 'seccion', 'dia', 'fecha_energizacion', 'id', 'el_as',
                        'el_ba', 'el_bi', 'el_ca', 'el_cl', 'el_fe', 'el_fe2',
                        'el_ni', 'el_pb', 'el_sb', 'voltajeCH',
                        'temperatura', 'agua', 'acido', 'corriente',
                        'an_ag', 'an_as', 'an_bi', 'an_ca', 'an_o2', 'an_pb',
                        'an_sb', 'estanque1', 'estanque2', 'cca',
                        'ccb', 'peso_anodo']

Data_Circuito.columns = ['fecha', 'voltajeSEC', 'temperatura', 'agua', 'acido', 'corriente',
                         'an_ag', 'an_as', 'an_bi', 'an_ca', 'an_o2', 'an_pb',
                         'an_sb', 'estanque1', 'estanque2', 'el_as', 'el_ba',
                         'el_bi', 'el_ca', 'el_cl', 'el_fe', 'el_fe2', 'el_ni',
                         'el_pb', 'el_sb']

# Se asumirá que el largo máximo de cosecha son 25 días
fecha_hoy = Data_Circuito['fecha'].iloc[-1] # última fecha
fecha_iteracion = pd.to_datetime(fecha_hoy, dayfirst=True, utc=True)
primera_fecha_consulta = fecha_iteracion - timedelta(days=25)
primera_fecha_consulta = date_to_string(primera_fecha_consulta)


# Aquí se debería consultar a cassandra
numero_ch = 14
n = 20
Data_Seccion = Data_Seccion[Data_Seccion['fecha'] >= primera_fecha_consulta].loc[numero_ch]
Data_Circuito = Data_Circuito[Data_Circuito['fecha'] >= primera_fecha_consulta].loc[numero_ch]

Vector_Seccion_Activa = np.ones(10, dtype=bool)


for i in range(8):

    DIAS, PREDICCION_CH, PREDICCION_SECC = Run_models(Vector_Seccion_Activa, Data_Circuito, fecha_iteracion, numero_ch,
                                                      Data_Seccion, net, outputs, x, net_eficiencia, outputs_eficiencia,
                                                      xef, net_comp_quim, outputs_comp_quim, xcq, net_rechazo,
                                                      outputs_rechazo, xrech, Max_min_output_cosecha,
                                                      Max_min_output_circuito, Max_min_input_cosecha,
                                                      Max_min_input_circuito)

    #print(PREDICCION_CH)
    #print(PREDICCION_SECC)
    # print(Vector_Seccion_Activa)
    # print(PREDICCION_CH)
    print(DIAS) ############################################################################# FALTA SUMAR DIAS EN CADA ITERACION ################
    Vector_Seccion_Activa = change_vector_seccion_activa(Vector_Seccion_Activa, DIAS, n)

    # Sumamos un día
    fecha_iteracion_past = fecha_iteracion
    primera_fecha = Data_Seccion['fecha'].iloc[0]
    fecha_iteracion = pd.to_datetime(fecha_iteracion, dayfirst=True, utc=True) + timedelta(days=1)
    f = str(fecha_iteracion)
    fecha_iteracion = f.split(' ')[0] + ' 03:00:00+0000'

    # Predicciones
    pred_circuito = Data_Circuito[Data_Circuito['fecha'] >= str(fecha_iteracion_past).split(' ')[0] + ' 03:00:00+0000'].copy()

    pred_circuito.loc[numero_ch, ['el_as', 'el_bi', 'el_sb']] = PREDICCION_CH.loc[14, 'el_as'], PREDICCION_CH.loc[14, 'el_bi'], \
                                                         PREDICCION_CH.loc[14, 'el_sb']
    pred_circuito['fecha'] = fecha_iteracion


    pred_seccion = Data_Seccion[Data_Seccion['fecha'] >=  str(fecha_iteracion_past).split(' ')[0] + ' 03:00:00+0000'].copy()

    pred_seccion.loc[numero_ch, ['el_as', 'el_bi', 'el_sb']] = PREDICCION_CH.loc[14, 'el_as'], PREDICCION_CH.loc[14, 'el_bi'], \
                                                                PREDICCION_CH.loc[14, 'el_sb']
    pred_seccion['fecha'] = fecha_iteracion
    pred_seccion['dia'] = pred_seccion['dia'] + 1

    # Se eliminan todos los datos de la primera fecha
    Data_Circuito = pd.concat([Data_Circuito[Data_Circuito['fecha'] > primera_fecha], pred_circuito], axis=0)
    Data_Seccion = pd.concat([Data_Seccion[Data_Seccion['fecha'] > primera_fecha], pred_seccion], axis=0)


    fecha_iteracion = pd.to_datetime(fecha_iteracion, dayfirst=True, utc=True)


    # #print(Data_Circuito)
    # Data_Circuito = Data_Circuito.reset_index(drop=False)
    # Data_Circuito = Data_Circuito.set_index('fecha')
    #
    # Data_Circuito.loc[f, ['el_as', 'el_bi', 'el_sb']] = PREDICCION_CH.loc[14, 'el_as'], PREDICCION_CH.loc[14, 'el_bi'], \
    #                                                     PREDICCION_CH.loc[14, 'el_sb']
    # Data_Circuito = Data_Circuito.reset_index(drop=False)
    # Data_Circuito = Data_Circuito.set_index('circuito', drop=True)

    # print(Data_Circuito.columns)
