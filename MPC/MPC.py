import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from datetime import datetime, timedelta
import os
import copy
import time
from pyswarm import pso
from pyswarms.single import GlobalBestPSO
from pyswarms.utils.plotters import (plot_cost_history, plot_contour, plot_surface)
from models import RNN_LSTM_Cosecha, RNN_LSTM_Circ, extractDigit, Run_models, string_to_date, date_to_string, \
    change_vector_seccion_activa, OptimalSolution, MyOptimizer
import warnings
warnings.filterwarnings("ignore")


class MPC:
    def __init__(self, path_models='../Empaquetado/Modelos/', path_data="../Empaquetado/Datos/",
                 predict_horizon=10, control_horizon=5, num_cv=2, num_mv=2, numero_ch=14, mv=['agua', 'acido'],
                 cv=['rechazo', 'eficiencia'], duracion_max_cosecha=20, limits={}, weights={}, refs=[]):
        self.path_models = path_models
        self.path_data = path_data
        self.predict_horizon = predict_horizon
        self.control_horizon = control_horizon
        self.mv = mv
        self.cv = cv
        self.limits = limits
        self.weights = weights
        self.refs = refs
        self.n = duracion_max_cosecha
        self.numero_ch = numero_ch
        self.num_cv = num_cv
        self.num_mv = num_mv
        self.load_models()
        self.load_data()

    def load_models(self):
        print('Loading models')
        # Creamos la RNN Electrolito
        self.x_circ, self.y_circ, self.net_circ, self.outputs_circ, self.training_op, self.saver_circ = RNN_LSTM_Circ()
        # Cargamos el modelo
        self.saver_circ.restore(self.net_circ, self.path_models + 'Electrolito_A1.ckpt')

        # Creamos la RNN Cosecha
        n_outputs = 1  # Eficiencia y Rechazo
        n_outputs_cq = 4  # Componentes Químicos

        self.xef, self.yef, self.net_eficiencia, self.outputs_eficiencia, self.training_op_efi, self.saver_efi = \
            RNN_LSTM_Cosecha(n_outputs)
        self.xrech, self.yrech, self.net_rechazo, self.outputs_rechazo, self.training_op_rech, self.saver_rech = \
            RNN_LSTM_Cosecha(n_outputs)
        self.xcq, self.ycq, self.net_comp_quim, self.outputs_comp_quim, self.training_op_cq, self.saver_cq = \
            RNN_LSTM_Cosecha(n_outputs_cq)

        # Cargamos los modelos
        self.saver_efi.restore(self.net_eficiencia, self.path_models + 'Eficiencia_a1.ckpt')
        self.saver_cq.restore(self.net_rechazo, self.path_models + 'Rechazo_a1.ckpt')
        self.saver_rech.restore(self.net_comp_quim, self.path_models + 'Comp_Quim_a1.ckpt')


    def load_data(self):
        files = os.listdir(self.path_data)
        files.sort()
        files = [self.path_data + file for file in files]

        # Cargamos datos del Circuito Hidraulico CH
        self.Data_Circuito = pd.read_csv(files[1], sep=';').sort_values(by='fecha')  # .reset_index()

        # Cargamos datos de la sección SECC
        self.Data_Seccion = pd.read_csv(files[0], sep=';').sort_values(by='fecha')  # .reset_index()

        # Valores para el MaxMin Scaler
        self.Max_min_input_cosecha = preprocessing.MinMaxScaler()
        self.Max_min_output_cosecha = preprocessing.MinMaxScaler()
        self.Max_min_input_circuito = preprocessing.MinMaxScaler()
        self.Max_min_output_circuito = preprocessing.MinMaxScaler()


        self.Max_min_input_cosecha.fit_transform(pd.read_csv(files[2], sep=';'))
        self.Max_min_output_cosecha.fit_transform(pd.read_csv(files[3], sep=';'))
        self.Max_min_input_circuito.fit_transform(pd.read_csv(files[4], sep=';'))
        self.Max_min_output_circuito.fit_transform(pd.read_csv(files[5], sep=';'))

        # Dejamos como índice los CH para los CH
        self.Data_Circuito = self.Data_Circuito.set_index('circuito', drop=True)
        # Dejamos como índice los CH para la SECC
        self.Data_Seccion = self.Data_Seccion.set_index('circuito', drop=True)

        self.Data_Seccion.columns = ['fecha', 'seccion', 'dia', 'fecha_energizacion', 'id', 'el_as',
                                'el_ba', 'el_bi', 'el_ca', 'el_cl', 'el_fe', 'el_fe2',
                                'el_ni', 'el_pb', 'el_sb', 'voltajeCH',
                                'temperatura', 'agua', 'acido', 'corriente',
                                'an_ag', 'an_as', 'an_bi', 'an_ca', 'an_o2', 'an_pb',
                                'an_sb', 'estanque1', 'estanque2', 'cca',
                                'ccb', 'peso_anodo']

        self.Data_Circuito.columns = ['fecha', 'voltajeSEC', 'temperatura', 'agua', 'acido', 'corriente',
                                 'an_ag', 'an_as', 'an_bi', 'an_ca', 'an_o2', 'an_pb',
                                 'an_sb', 'estanque1', 'estanque2', 'el_as', 'el_ba',
                                 'el_bi', 'el_ca', 'el_cl', 'el_fe', 'el_fe2', 'el_ni',
                                 'el_pb', 'el_sb']

    def iterate_models(self, Data_Circuito, Data_Seccion, fecha_iteracion, numero_ch=14, n=20):
        Vector_Seccion_Activa = np.ones(10, dtype=bool)
        DIAS, PREDICCION_CH, PREDICCION_SECC = Run_models(Vector_Seccion_Activa, Data_Circuito, fecha_iteracion,
                                                          numero_ch, Data_Seccion, self.net_circ, self.outputs_circ,
                                                          self.x_circ, self.net_eficiencia, self.outputs_eficiencia,
                                                          self.xef, self.net_comp_quim, self.outputs_comp_quim, self.xcq,
                                                          self.net_rechazo, self.outputs_rechazo, self.xrech, self.Max_min_output_cosecha,
                                                          self.Max_min_output_circuito, self.Max_min_input_cosecha,
                                                          self.Max_min_input_circuito)

        Vector_Seccion_Activa = change_vector_seccion_activa(Vector_Seccion_Activa, DIAS, n)
        # Sumamos un día
        fecha_iteracion_past = fecha_iteracion
        primera_fecha = Data_Seccion['fecha'].iloc[0]
        fecha_iteracion = pd.to_datetime(fecha_iteracion, dayfirst=True, utc=True) + timedelta(days=1)
        f = str(fecha_iteracion)
        fecha_iteracion = f.split(' ')[0] + ' 03:00:00+0000'

        # Predicciones
        pred_circuito = Data_Circuito[
            Data_Circuito['fecha'] >= str(fecha_iteracion_past).split(' ')[0] + ' 03:00:00+0000'].copy()

        pred_circuito.loc[numero_ch, ['el_as', 'el_bi', 'el_sb']] = PREDICCION_CH.loc[14, 'el_as'], PREDICCION_CH.loc[
            14, 'el_bi'], \
                                                                    PREDICCION_CH.loc[14, 'el_sb']
        pred_circuito['fecha'] = fecha_iteracion

        pred_seccion = Data_Seccion[
            Data_Seccion['fecha'] >= str(fecha_iteracion_past).split(' ')[0] + ' 03:00:00+0000'].copy()

        pred_seccion.loc[numero_ch, ['el_as', 'el_bi', 'el_sb']] = PREDICCION_CH.loc[14, 'el_as'], PREDICCION_CH.loc[
            14, 'el_bi'], \
                                                                   PREDICCION_CH.loc[14, 'el_sb']
        pred_seccion['fecha'] = fecha_iteracion
        pred_seccion['dia'] = pred_seccion['dia'] + 1

        # Se eliminan todos los datos de la primera fecha
        Data_Circuito = pd.concat([Data_Circuito[Data_Circuito['fecha'] > primera_fecha], pred_circuito], axis=0)
        Data_Seccion = pd.concat([Data_Seccion[Data_Seccion['fecha'] > primera_fecha], pred_seccion], axis=0)

        fecha_iteracion = pd.to_datetime(fecha_iteracion, dayfirst=True, utc=True)


    def objective_function(self, x, Data_Circuito, Data_Seccion, fecha_iteracion, numero_ch=14, simulation=False):

        Vector_Seccion_Activa = np.ones(10, dtype=bool)
        preds_seccion_list = []
        mv_list = []
        Data_Circuito_original = copy.deepcopy(Data_Circuito)
        Data_Seccion_original = copy.deepcopy(Data_Seccion)

        for i in range(self.predict_horizon):

            # Ejecución de una acción de contol
            if i < self.control_horizon:
                fecha_iteracion_string = str(fecha_iteracion)
                fecha_iteracion_string = fecha_iteracion_string.split(' ')[0] + ' 03:00:00+0000'
                pos = len(self.mv)*i # En este caso tomará los valores. 0, 2, 4 ... control horizon*2
                aux_mv_list = []
                for num_mv in range(self.num_mv):
                    x1_aux = Data_Circuito[self.mv[num_mv]].iloc[-2] + x[pos + num_mv]
                    x1_aux = max(min(x1_aux, self.limits['mvHighLimits'][num_mv]), self.limits['mvLowLimits'][num_mv]) # Saturaciones
                    Data_Circuito[self.mv[num_mv]].iloc[-1] = x1_aux
                    aux = Data_Seccion[Data_Seccion['fecha'] >= fecha_iteracion_string]
                    aux[self.mv[num_mv]] = x1_aux
                    Data_Seccion[Data_Seccion['fecha'] >= fecha_iteracion_string] = aux
                    aux_mv_list.append(Data_Circuito[self.mv[num_mv]].iloc[-1] - Data_Circuito[self.mv[num_mv]].iloc[-2]) # Real mv
                mv_list.append(aux_mv_list)

            # Predicciones
            DIAS, PREDICCION_CH, PREDICCION_SECC = Run_models(Vector_Seccion_Activa, Data_Circuito, fecha_iteracion,
                                                              numero_ch, Data_Seccion, self.net_circ, self.outputs_circ,
                                                              self.x_circ, self.net_eficiencia, self.outputs_eficiencia,
                                                              self.xef, self.net_comp_quim, self.outputs_comp_quim,
                                                              self.xcq,
                                                              self.net_rechazo, self.outputs_rechazo, self.xrech,
                                                              self.Max_min_output_cosecha,
                                                              self.Max_min_output_circuito, self.Max_min_input_cosecha,
                                                              self.Max_min_input_circuito)

            Vector_Seccion_Activa = change_vector_seccion_activa(Vector_Seccion_Activa, DIAS, self.n)

            # Sumamos un día
            fecha_iteracion_past = fecha_iteracion
            primera_fecha = Data_Seccion['fecha'].iloc[0]
            fecha_iteracion = pd.to_datetime(fecha_iteracion, dayfirst=True, utc=True) + timedelta(days=1)
            f = str(fecha_iteracion)
            fecha_iteracion = f.split(' ')[0] + ' 03:00:00+0000'

            # Predicciones
            pred_circuito = Data_Circuito[
                Data_Circuito['fecha'] >= str(fecha_iteracion_past).split(' ')[0] + ' 03:00:00+0000'].copy()

            pred_circuito.loc[numero_ch, ['el_as', 'el_bi', 'el_sb']] = PREDICCION_CH.loc[14, 'el_as'], \
                                                                        PREDICCION_CH.loc[
                                                                            14, 'el_bi'], \
                                                                        PREDICCION_CH.loc[14, 'el_sb']
            pred_circuito['fecha'] = fecha_iteracion

            pred_seccion = Data_Seccion[
                Data_Seccion['fecha'] >= str(fecha_iteracion_past).split(' ')[0] + ' 03:00:00+0000'].copy()

            pred_seccion.loc[numero_ch, ['el_as', 'el_bi', 'el_sb']] = PREDICCION_CH.loc[14, 'el_as'], \
                                                                       PREDICCION_CH.loc[
                                                                           14, 'el_bi'], \
                                                                       PREDICCION_CH.loc[14, 'el_sb']
            pred_seccion['fecha'] = fecha_iteracion
            pred_seccion['dia'] = pred_seccion['dia'] + 1

            # Se eliminan todos los datos de la primera fecha
            Data_Circuito = pd.concat([Data_Circuito[Data_Circuito['fecha'] > primera_fecha], pred_circuito], axis=0)
            Data_Seccion = pd.concat([Data_Seccion[Data_Seccion['fecha'] > primera_fecha], pred_seccion], axis=0)

            fecha_iteracion = pd.to_datetime(fecha_iteracion, dayfirst=True, utc=True)

            # Se guardan las predicciones de las secciones
            preds_seccion_list.append(PREDICCION_SECC)

            # Si todas las cosechas acabaron, hay que terminar de predecir
            bool_array = list(np.array(DIAS['Dias Activa']).astype(int) == self.n)
            if bool_array.count(True) == len(bool_array):
                break

        # Se concatenan las predicciones y se ordenan en un diccionario
        preds_seccion_list = pd.concat(preds_seccion_list, axis=0)

        preds_seccion_list = preds_seccion_list.groupby('id')
        dicto_preds = {}
        shapes = []
        # Se guarda en un diccionario las predicciones de cada sección por separado
        for key, value in preds_seccion_list:
            shapes.append(value.shape)
            dicto_preds[key] = value

        # Se calcula el costo
        max_shape = max(shapes, key=lambda x: x[0])
        cost = self.cost_function(dicto_preds, mv_list, max_shape)

        if simulation:
            _, mv_no_incremental = self.calculate_x_no_incremental(mv_list, Data_Circuito_original)
            return mv_list, mv_no_incremental, dicto_preds, cost
        else:
            return cost

    def calculate_x_no_incremental(self, x, Data_Circuito):
        x_out = []
        last_vals = []
        x = np.array(x)

        for num_mv in range(self.num_mv):
            last_vals.append(Data_Circuito[self.mv[num_mv]].iloc[-2]) # Valores iniciales
        first_vals = copy.deepcopy(last_vals)
        for i in range(self.control_horizon):
            x_aux = []
            for num_mv in range(self.num_mv):
                last_vals[num_mv] = last_vals[num_mv] + x[i, num_mv]
                x_aux.append(last_vals[num_mv])
            x_out.append(x_aux)
        return first_vals, x_out


    def objective_function_v2(self, x, Data_Circuito, Data_Seccion, fecha_iteracion, numero_ch=14):
        J_list = []
        Data_Circuito_original = Data_Circuito
        Data_Seccion_original = Data_Seccion
        fecha_iteracion_original = fecha_iteracion
        for p in range(x.shape[0]):
            #print('{}|{}'.format(p + 1, x.shape[0]))
            Data_Circuito = Data_Circuito_original.copy()
            Data_Seccion = Data_Seccion_original.copy()
            fecha_iteracion = copy.deepcopy(fecha_iteracion_original)
            Vector_Seccion_Activa = np.ones(10, dtype=bool)
            preds_seccion_list = []
            mv_list = []
            for i in range(self.predict_horizon):

                # Ejecución de una acción de contol
                if i < self.control_horizon:
                    fecha_iteracion_string = str(fecha_iteracion)
                    fecha_iteracion_string = fecha_iteracion_string.split(' ')[0] + ' 03:00:00+0000'
                    pos = len(self.mv) * i  # En este caso tomará los valores. 0, 2, 4 ... control horizon*2
                    aux_mv_list = []
                    for num_mv in range(self.num_mv):
                        x1_aux = Data_Circuito[self.mv[num_mv]].iloc[-2] + x[p, pos + num_mv]
                        x1_aux = max(min(x1_aux, self.limits['mvHighLimits'][num_mv]),
                                     self.limits['mvLowLimits'][num_mv])  # Saturaciones
                        Data_Circuito[self.mv[num_mv]].iloc[-1] = x1_aux
                        aux = Data_Seccion[Data_Seccion['fecha'] >= fecha_iteracion_string]
                        aux[self.mv[num_mv]] = x1_aux
                        Data_Seccion[Data_Seccion['fecha'] >= fecha_iteracion_string] = aux
                        aux_mv_list.append(
                            Data_Circuito[self.mv[num_mv]].iloc[-1] - Data_Circuito[self.mv[num_mv]].iloc[-2])  # Real mv
                    mv_list.append(aux_mv_list)

                # Predicciones
                DIAS, PREDICCION_CH, PREDICCION_SECC = Run_models(Vector_Seccion_Activa, Data_Circuito, fecha_iteracion,
                                                                  numero_ch, Data_Seccion, self.net_circ, self.outputs_circ,
                                                                  self.x_circ, self.net_eficiencia, self.outputs_eficiencia,
                                                                  self.xef, self.net_comp_quim, self.outputs_comp_quim,
                                                                  self.xcq,
                                                                  self.net_rechazo, self.outputs_rechazo, self.xrech,
                                                                  self.Max_min_output_cosecha,
                                                                  self.Max_min_output_circuito, self.Max_min_input_cosecha,
                                                                  self.Max_min_input_circuito)

                Vector_Seccion_Activa = change_vector_seccion_activa(Vector_Seccion_Activa, DIAS, self.n)

                # Sumamos un día
                fecha_iteracion_past = fecha_iteracion
                primera_fecha = Data_Seccion['fecha'].iloc[0]
                fecha_iteracion = pd.to_datetime(fecha_iteracion, dayfirst=True, utc=True) + timedelta(days=1)
                f = str(fecha_iteracion)
                fecha_iteracion = f.split(' ')[0] + ' 03:00:00+0000'

                # Predicciones
                pred_circuito = Data_Circuito[
                    Data_Circuito['fecha'] >= str(fecha_iteracion_past).split(' ')[0] + ' 03:00:00+0000'].copy()

                pred_circuito.loc[numero_ch, ['el_as', 'el_bi', 'el_sb']] = PREDICCION_CH.loc[14, 'el_as'], \
                                                                            PREDICCION_CH.loc[
                                                                                14, 'el_bi'], \
                                                                            PREDICCION_CH.loc[14, 'el_sb']
                pred_circuito['fecha'] = fecha_iteracion

                pred_seccion = Data_Seccion[
                    Data_Seccion['fecha'] >= str(fecha_iteracion_past).split(' ')[0] + ' 03:00:00+0000'].copy()

                pred_seccion.loc[numero_ch, ['el_as', 'el_bi', 'el_sb']] = PREDICCION_CH.loc[14, 'el_as'], \
                                                                           PREDICCION_CH.loc[
                                                                               14, 'el_bi'], \
                                                                           PREDICCION_CH.loc[14, 'el_sb']
                pred_seccion['fecha'] = fecha_iteracion
                pred_seccion['dia'] = pred_seccion['dia'] + 1

                # Se eliminan todos los datos de la primera fecha
                Data_Circuito = pd.concat([Data_Circuito[Data_Circuito['fecha'] > primera_fecha], pred_circuito], axis=0)
                Data_Seccion = pd.concat([Data_Seccion[Data_Seccion['fecha'] > primera_fecha], pred_seccion], axis=0)

                fecha_iteracion = pd.to_datetime(fecha_iteracion, dayfirst=True, utc=True)

                # Se guardan las predicciones de las secciones
                preds_seccion_list.append(PREDICCION_SECC)

                # Si todas las cosechas acabaron, hay que terminar de predecir
                bool_array = list(np.array(DIAS['Dias Activa']).astype(int) == self.n)
                if bool_array.count(True) == len(bool_array):
                    break

            # Se concatenan las predicciones y se ordenan en un diccionario
            preds_seccion_list = pd.concat(preds_seccion_list, axis=0)

            preds_seccion_list = preds_seccion_list.groupby('id')
            dicto_preds = {}
            shapes = []
            # Se guarda en un diccionario las predicciones de cada sección por separado
            for key, value in preds_seccion_list:
                shapes.append(value.shape)
                dicto_preds[key] = value

            # Se calcula el costo
            max_shape = max(shapes, key=lambda x: x[0])
            cost = self.cost_function(dicto_preds, mv_list, max_shape)
            J_list.append(cost)
        J_list = np.array(J_list)
        return J_list

    def cost_function(self, dicto_preds, mv_list, max_shape):
        cv_ranges = self.Max_min_output_cosecha.data_range_
        mv_list = np.array(mv_list)

        refs = np.repeat(self.refs.reshape(1, -1), repeats=max_shape[0], axis=0)
        qWeights = np.repeat(self.weights['qWeights'].reshape(1, -1), repeats=max_shape[0], axis=0)/cv_ranges# Pesos de las variables controladas al llegar a la referencia
        rWeights = np.repeat(self.weights['rWeights'].reshape(1, -1), repeats=mv_list.shape[0], axis=0)
        terminalWeights = self.weights['terminalWeights'].reshape(1, -1)/cv_ranges
        lambdaWeights = np.repeat(self.weights['lambdaWeights'].reshape(1, -1), repeats=max_shape[0], axis=0)/cv_ranges

        # Cálculo del costo
        J = 0
        Nu = self.control_horizon
        mv_cost = np.sum(rWeights / Nu * mv_list**2) # Se calcula solo una vez porque para todas las secciones es lo mismo
        for key, value in dicto_preds.items():
            Ny = value.shape[0]
            value = np.array(value)
            error = value - refs[:Ny, :]

            # Costos
            ref_cost = np.sum(qWeights[:Ny, :]/Ny*error**2) # Se divide por el largo de cada una de las cosechas para ser parejos
            terminal_cost = np.sum(error[-1:, :]**2*terminalWeights)
            lambda_cost = (value > self.limits['cvHighLimits']) + (value < self.limits['cvLowLimits'])
            lambda_cost = np.sum(lambda_cost*lambdaWeights[:Ny, :]/Ny*np.abs(value)) # Si hay violación True=1

            J += ref_cost + mv_cost + terminal_cost + lambda_cost

        return J


    def optimize(self, Data_Circuito, Data_Seccion, fecha_iteracion, numero_ch=14, plot=False):
        lower_bound = list(self.limits['dMvLowLimits'])*self.control_horizon
        upper_bound = list(self.limits['dMvHighLimits'])*self.control_horizon

        dict_objective_func = {'Data_Circuito': Data_Circuito,
                               'Data_Seccion': Data_Seccion,
                               'fecha_iteracion': fecha_iteracion,
                               'numero_ch': numero_ch}

        # Otro optimizador
        #xopt, fopt = pso(self.objective_function, lower_bound, upper_bound, ieqcons=[], f_ieqcons=None, args=(), kwargs=dict_objective_func,
        #    swarmsize=5, omega=0.5, phip=0.5, phig=0.5, maxiter=10, minstep=1e-8,
        #    minfunc=1e-8, debug=False)
        #print('Optimals')
        #print(xopt)
        #print(fopt)

        options = {'c1': 0.5, 'c2': 0.3, 'w': 0.9}
        bounds = (np.array(lower_bound), np.array(upper_bound))
        optimizer = GlobalBestPSO(n_particles=50, bounds=bounds, dimensions=self.control_horizon*self.num_mv, options=options)
        cost, pos = optimizer.optimize(self.objective_function_v2, iters=30, **dict_objective_func)
        if plot:
            plot_cost_history(cost_history=optimizer.cost_history)
            plt.show()

        # Simulacion para obtener los valores reales
        mv_list, mv_no_incremental, dicto_preds, cost = self.objective_function(pos, Data_Circuito, Data_Seccion, fecha_iteracion,
                                                                                numero_ch, simulation=True)

        mv_list = np.array(mv_list)
        mv_no_incremental = np.array(mv_no_incremental)
        print('Cost: {}'.format(cost))
        print('MV: {}'.format(mv_list))
        print('Mv no incremental: {}'.format(mv_no_incremental))
        print('Dicto preds: {}'.format(dicto_preds))

        return mv_list, mv_no_incremental, dicto_preds, cost







if __name__ == '__main__':

    ######################################### Limits ###################################################################
    cvLowLimits = np.array([84, -1, -1, -1, -1, -1], float)
    cvHighLimits = np.array([103, 96, 18, 1.6, 11, 1], float)

    mvLowLimits = np.array([0., 0.], float)
    mvHighLimits = np.array([10, 10], float)

    deltaMVLowLimits = np.array([-2, -2], float)
    deltaMVHighLimits = -1 * deltaMVLowLimits.copy()

    limits = {'cvLowLimits': cvLowLimits,
              'cvHighLimits': cvHighLimits,
              'mvLowLimits': mvLowLimits,
              'mvHighLimits': mvHighLimits,
              'dMvLowLimits': deltaMVLowLimits,
              'dMvHighLimits': deltaMVHighLimits}

    ############################################### Costs ##############################################################
    costFactor = 1e-0

    # Costs of cv
    qWeights = np.array([1000, 100, 10, 10, 10, 10]) * costFactor  # 1e0 1e2 1e2

    # Costs of dmv
    rWeights = np.array([100, 100]) * costFactor  # 1e1, 5e-2

    # Terminal weights
    terminalWeights = np.array([1000, 100, 10, 10, 10, 10]) * costFactor

    # Violations
    lambdaWeights = np.array([1e7, 1e7, 1e2, 1e2, 1e2, 1e2]) * costFactor

    weights = {'qWeights': qWeights,
               'rWeights': rWeights,
               'terminalWeights': terminalWeights,
               'lambdaWeights': lambdaWeights}

    ################################################# References #######################################################
    Refs = np.array([100, 0, 0, 0, 0, 0]) # Referencias


    ################################################# Instantiation ###################################################
    # Instanciación del MPC
    n = 20
    mpc = MPC(control_horizon=5, predict_horizon=n, num_cv=2, num_mv=2, numero_ch=14, mv=['agua', 'acido'],
            duracion_max_cosecha=n, limits=limits, weights=weights, refs=Refs)


    ##################################################### Simulation ###################################################

    # Se asumirá que el largo máximo de cosecha son 25 días
    fecha_hoy = mpc.Data_Circuito['fecha'].iloc[-1]  # última fecha
    fecha_iteracion = pd.to_datetime(fecha_hoy, dayfirst=True, utc=True)
    primera_fecha_consulta = fecha_iteracion - timedelta(days=25)
    primera_fecha_consulta = date_to_string(primera_fecha_consulta)
    mpc.Data_Seccion = mpc.Data_Seccion[mpc.Data_Seccion['fecha'] >= primera_fecha_consulta].loc[mpc.numero_ch]
    mpc.Data_Circuito = mpc.Data_Circuito[mpc.Data_Circuito['fecha'] >= primera_fecha_consulta].loc[mpc.numero_ch]

    #cost = mpc.objective_function([0.5, 0.5, 0.3, 0.3, 0.1, 0.1], mpc.Data_Circuito, mpc.Data_Seccion, fecha_iteracion)
    mv_list, mv_no_incremental, dicto_preds, cost = mpc.optimize(mpc.Data_Circuito, mpc.Data_Seccion, fecha_iteracion)
