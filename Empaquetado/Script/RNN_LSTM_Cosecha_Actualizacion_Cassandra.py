
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import os
import glob
import matplotlib
import matplotlib.pyplot as plt
import time
#from time import time
from datetime import datetime,timedelta
import tensorflow as tf
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import random
#import seaborn as sns
from sklearn.metrics import mean_squared_error


from cassandra.cluster import Cluster
from cassandra import timestamps


# In[2]:


#### Arquitectura de RNN LSTM ####
#parametros Cassandra
def pandas_factory(colnames, rows):
    return pd.DataFrame(rows, columns=colnames)
cluster = Cluster();
session = cluster.connect('piloto');
session.row_factory = pandas_factory
session.default_fetch_size = None


def querys(query):
    rows = session.execute(query)
    rows = rows._current_rows
    rows = rows.iloc[::-1]
    rows = rows.reset_index(drop=True)
    return rows

# Cosecha
def RNN_LSTM_Cosecha(n_outputs):
    
    #Parametros
    n_neurons= 50
    n_inputs = 30
    n_layers = 2
    learning_rate = 0.001
    #Arquitectura
    tf.reset_default_graph()
    x=tf.placeholder(tf.float32,[None,None,n_inputs])
    y=tf.placeholder(tf.float32,[None,None,n_outputs])
    cell = tf.contrib.rnn.MultiRNNCell([tf.contrib.rnn.LSTMCell(n_neurons) for i in range(n_layers)])
    cell_wrapped = tf.contrib.rnn.OutputProjectionWrapper(cell, output_size=n_outputs)
    outputs, states = tf.nn.dynamic_rnn(cell_wrapped, x, dtype=tf.float32)
    loss = tf.reduce_mean(tf.square(outputs - y))
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    training_op = optimizer.minimize(loss)
    #Inicialización
    init = tf.global_variables_initializer()
    net = tf.Session()
    net.run(init)
    saver = tf.train.Saver()
    
    return x,y,net,outputs,training_op,saver

# Electrolito
def RNN_LSTM_Circ():
    
    #Parametros
    n_neurons= 50
    n_inputs = 24
    n_outputs = 3
    batch_size = 20
    n_layers = 2
    learning_rate = 0.001
    horizonte = 1
    #Arquitectura
    tf.reset_default_graph()
    x=tf.placeholder(tf.float32,[None,batch_size,n_inputs])
    y=tf.placeholder(tf.float32,[None,batch_size,n_outputs])
    cell = tf.contrib.rnn.MultiRNNCell([tf.contrib.rnn.LSTMCell(n_neurons) for i in range(n_layers)])
    cell_wrapped = tf.contrib.rnn.OutputProjectionWrapper(cell, output_size=n_outputs)
    outputs, states = tf.nn.dynamic_rnn(cell_wrapped, x, dtype=tf.float32)
    loss = tf.reduce_mean(tf.square(outputs - y))
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    training_op = optimizer.minimize(loss)
    #Inicialización
    init = tf.global_variables_initializer()
    net = tf.Session()
    net.run(init)
    saver = tf.train.Saver()
    
    return x,y,net,outputs,training_op,saver



# In[3]:


def extractDigit(number, place):
    temp = number / place
    value=int(round(temp % 10))
    if value==0:
        value=value+10
    return value


# In[4]:


#Directorio modelos
#path_load = 'E:\\Proyecto FONDEF 17IT10011M\\actividades\\19_empaquetado\\Empaquetado\\Modelos\\'
path_load = 'C:\\Users\\mario\\it1710011\\it17m10011\\Empaquetado\\Modelos\\'
#path_load = 'D:\\FondefR\\2802\\Empaquetado\\Modelos\\'

#Creamos la RNN Electrolito
x,y,net,outputs,training_op,saver = RNN_LSTM_Circ()
# Cargamos el modelo
saver.restore(net, path_load+'Electrolito_A1.ckpt')


#Creamos la RNN Cosecha
n_outputs = 1 # Eficiencia y Rechazo
n_outputs_cq = 4 #Componentes Químicos
xef,yef,net_eficiencia,outputs_eficiencia,training_op_efi,saver_efi = RNN_LSTM_Cosecha(n_outputs)
xrech,yrech,net_rechazo,outputs_rechazo,training_op_rech,saver_rech = RNN_LSTM_Cosecha(n_outputs)
xcq,ycq,net_comp_quim,outputs_comp_quim,training_op_cq,saver_cq = RNN_LSTM_Cosecha(n_outputs_cq)
# Cargamos los modelos
saver_efi.restore(net_eficiencia, path_load+'Eficiencia_a1.ckpt')
saver_cq.restore(net_rechazo, path_load+'Rechazo_a1.ckpt')
saver_rech.restore(net_comp_quim, path_load+'Comp_Quim_a1.ckpt')


# In[12]:


def Run_models(Vector_Seccion_Activa,Data_Circuito,fecha_iteracion,numero_ch,Data_Seccion,net,outputs,x,net_eficiencia,outputs_eficiencia,xef,net_comp_quim,outputs_comp_quim,xcq,net_rechazo,outputs_rechazo,xrech,Max_min_output_cosecha,Max_min_output_circuito,Max_min_input_cosecha,Max_min_input_circuito):
    i=numero_ch

    Variables_predecir_electrolito = ['el_as','el_bi', 'el_sb']
    Secciones_CH=np.array([1,2,3,4,5,6,7,8,9,10])

    DF_CH_SEC=querys("select * from piloto.electrolito_in where circuito=%d and fecha> '%s' and fecha<= '%s' allow filtering;"% (i,fecha_iteracion-timedelta(19), fecha_iteracion+timedelta(1/8)))
    DF_CH_SEC.drop('circuito',axis=1, inplace=True)
    # Empareja los tags de las mismas variables

    DF_CH_SEC.columns = ['fecha', 'voltajeSEC', 'temperatura', 'agua', 'acido', 'corriente',
       'an_ag', 'an_as', 'an_bi', 'an_ca', 'an_o2', 'an_pb',
       'an_sb', 'estanque1', 'estanque2', 'el_as', 'el_ba',
       'el_bi', 'el_ca', 'el_cl', 'el_fe', 'el_fe2', 'el_ni',
       'el_pb', 'el_sb']
    DF_INPUT_CH=DF_CH_SEC.drop('fecha',axis=1)
    #print(DF_CH_SEC)
    # Normalizar a MinMax y Predecir
    prediccion_ch=net.run(outputs, feed_dict={x: Max_min_input_circuito.transform(DF_INPUT_CH).reshape([-1, DF_INPUT_CH.shape[0], DF_INPUT_CH.shape[1]])})
    prediccion_ch=prediccion_ch.reshape([prediccion_ch.shape[1],prediccion_ch.shape[2]])[-1,:]

    # DF de la predicción de electrolito (escalada)
    PREDICCION_CH = pd.DataFrame(prediccion_ch,index=Variables_predecir_electrolito,columns=[i]).T

    activas=querys("select circuito,seccion, dia, id, fecha_energizacion from piloto.cosechas_in where fecha='%s' ALLOW FILTERING;"%(fecha_iteracion+timedelta(1/8)))
    activas=activas[activas['circuito']==i]
    Secciones_Activas_Dia=activas.seccion.values


    Secciones_A_Predecir=np.intersect1d(Secciones_CH[Vector_Seccion_Activa], Secciones_Activas_Dia)
    #print(Secciones_A_Predecir)
    DIAS = [] #Acumular los dias que quedan
    Acumulador=[] # Acumulador de DF
    for j in Secciones_A_Predecir: # Iterar por secciones validas
        id_seccion=activas.id[activas.seccion==j].values[0]
        dia=activas.dia[activas.seccion==j].values[0]



        # Obtendo los datos con la cosecha actual (filtro las cosechas que ya ocurrieron)
        DF_SECCION_V1=querys("select fecha, seccion, dia, fecha_energizacion, id,AA_EL_AS,AB_EL_BA ,AC_EL_BI ,AD_EL_CA ,AE_EL_CL ,AF_EL_FE ,AG_EL_FE2 ,AH_EL_NI ,AI_EL_PB ,AJ_EL_SB ,AK_PR_VOLTAJE ,AL_PR_TEMPERATURA ,AM_PR_AGUA ,AN_PR_ACIDO ,AO_PR_CORRIENTE ,AP_AN_AG ,AQ_AN_AS ,AR_AN_BI ,AS_AN_CA ,AT_AN_O2 ,AU_AN_PB ,AV_AN_SB ,AW_PR_ESTANQUE7 ,AX_PR_ESTANQUE8 ,AY_PR_CCA ,AZ_PR_CCB ,BA_PESO_ANODO from cosechas_in where circuito= %d and seccion=%d and dia>0 and dia<=%d and id=%d allow filtering;"%(i, j, dia, id_seccion))
        DF_SECCION_V1.columns = ['fecha', 'seccion', 'dia', 'fecha_energizacion', 'id', 'el_as',
       'el_ba', 'el_bi', 'el_ca', 'el_cl', 'el_fe', 'el_fe2',
       'el_ni', 'el_pb', 'el_sb', 'voltajeCH',
       'temperatura', 'agua', 'acido', 'corriente',
       'an_ag', 'an_as', 'an_bi', 'an_ca', 'an_o2', 'an_pb',
       'an_sb', 'estanque1', 'estanque2', 'cca',
       'ccb', 'peso_anodo']
        DF_CH_SEC = DF_INPUT_CH.iloc[-DF_SECCION_V1.shape[0]:,:]
        #print ('print DF_SECCION')
        #print(DF_SECCION_V1)
        DIAS.append(DF_SECCION_V1['dia'].max())# Duracion actual de la cosecha
        index_variable_seccion = DF_SECCION_V1.columns[5:] # Variables
       # print(index_variable_seccion)
        # Actualizar los datos de cosecha con los datos a nivel de circuito

        #print(DF_SECCION_V1)   
        #print(DF_CH_SEC)
        filtro_variables=np.intersect1d(DF_SECCION_V1.columns[5:], DF_CH_SEC.columns[1:])

        #print(len(filtro_variables))
        #print(DF_CH_SEC[filtro_variables].shape)
        #print(DF_SECCION_V1[filtro_variables].shape)

        #print(DF_CH_SEC[filtro_variables])
        #print(DF_SECCION_V1[filtro_variables])

        DF_SECCION_V1[filtro_variables]=DF_CH_SEC[filtro_variables].values

        # Primer Input con las variables de la cosech

        DF_INPUT_SEC_1 = DF_SECCION_V1[DF_SECCION_V1.columns[5:]]
        DF_INPUT_SEC_1=DF_INPUT_SEC_1.reset_index(drop=True)
        #Normalizar a MinMax
        DF_INPUT_SEC_1=pd.DataFrame(Max_min_input_cosecha.transform(DF_INPUT_SEC_1),index=DF_INPUT_SEC_1.index,columns=DF_INPUT_SEC_1.columns)


        # Segundo Input con las predicciones de electrolito
        DF_INPUT_SEC_2 = pd.concat([DF_INPUT_SEC_1[Variables_predecir_electrolito][1:],PREDICCION_CH],ignore_index=True)
        DF_INPUT_SEC_2.columns='Out_'+DF_INPUT_SEC_2.columns

        #Concateno Input 1 e Input 2
        x_test=pd.concat([DF_INPUT_SEC_1,DF_INPUT_SEC_2],axis=1,sort=False)
        x_test=x_test.values.reshape([-1,x_test.shape[0],x_test.shape[1]])

        #Prediccion Eficiencia
        yf=net_eficiencia.run(outputs_eficiencia, feed_dict={xef: x_test})[0,-1,0]
        #Prediccion Comp Quimicos
        yc=net_comp_quim.run(outputs_comp_quim, feed_dict={xcq: x_test})[0,-1,:]
        #Prediccion Rechazo
        yr=net_rechazo.run(outputs_rechazo, feed_dict={xrech: x_test})[0,-1,0]


        # DF por SECCION
        PREDICCION_SEC_1=pd.DataFrame(np.array([yf,yr,yc[0],yc[1],yc[2],yc[3]]),columns=[id_seccion],index=['Eficiencia','Rechazo','Ag','As','S','Sb']).T
        # SE GUARDA en una lista
        Acumulador.append(PREDICCION_SEC_1)
    # DF de las prediccion por sección DEL Circuito Hidraulico
    PREDICCION_SECC=pd.concat(Acumulador)

    #PREDICCION_SECC.reindex(index = newindex)
    # Formo DF de Días que lleva activa
    DIAS = pd.DataFrame(DIAS,index=PREDICCION_SECC.index,columns=['Dias Activa'])
    # Reescalo la prediccion del electrolito
    PREDICCION_CH = pd.DataFrame(Max_min_output_circuito.inverse_transform(PREDICCION_CH),index=PREDICCION_CH.index,columns=PREDICCION_CH.columns)
    # Reescalo la predicción por cosecha/seccion
    PREDICCION_SECC = pd.DataFrame(Max_min_output_cosecha.inverse_transform(PREDICCION_SECC),index=PREDICCION_SECC.index,columns=PREDICCION_SECC.columns)

    return DIAS,PREDICCION_CH,PREDICCION_SECC


# In[20]:


#Cargamos los Datos
#my_dir =  "E:\\Proyecto FONDEF 17IT10011M\\actividades\\19_empaquetado\\Empaquetado\\Datos"
my_dir =  "C:\\Users\\mario\\it1710011\\it17m10011\\Empaquetado\\Datos"
#my_dir = "D:\\FondefR\\2802\\Empaquetado\\Datos"
os.chdir( my_dir )
files = glob.glob('**.csv')

#Cargamos datos del Circuito Hidraulico CH
Data_Circuito = pd.read_csv(files[1],sep=';')
#Cargamos datos de la sección SECC
Data_Seccion = pd.read_csv(files[0],sep=';')

#Valores para el MaxMin Scaler
Max_min_input_cosecha = preprocessing.MinMaxScaler()
Max_min_output_cosecha = preprocessing.MinMaxScaler()
Max_min_input_circuito = preprocessing.MinMaxScaler()
Max_min_output_circuito = preprocessing.MinMaxScaler()

Max_min_input_cosecha.fit_transform(pd.read_csv(files[2],sep=';'))
Max_min_output_cosecha.fit_transform(pd.read_csv(files[3],sep=';'))
Max_min_input_circuito.fit_transform(pd.read_csv(files[4],sep=';'))
Max_min_output_circuito.fit_transform(pd.read_csv(files[5],sep=';'))


# Dejamos como índice los CH para los CH
Data_Circuito=Data_Circuito.set_index('circuito',drop=True)
# Dejamos como índice los CH para la SECC
Data_Seccion = Data_Seccion.set_index('circuito',drop=True)

# Empareja los tags de las mismas variables
Data_Seccion.columns = ['fecha', 'seccion', 'dia', 'fecha_energizacion', 'id', 'el_as',
       'el_ba', 'el_bi', 'el_ca', 'el_cl', 'el_fe', 'el_fe2',
       'el_ni', 'el_pb', 'el_sb', 'voltajeCH',
       'temperatura', 'agua', 'acido', 'corriente',
       'an_ag', 'an_as', 'an_bi', 'an_ca', 'an_o2', 'an_pb',
       'an_sb', 'estanque1', 'estanque2', 'cca',
       'ccb', 'peso_anodo']

Data_Circuito.columns = ['fecha', 'voltajeSEC', 'temperatura', 'agua', 'acido', 'corriente',
       'an_ag', 'an_as', 'an_bi', 'an_ca', 'an_o2', 'an_pb',
       'an_sb', 'estanque1', 'estanque2', 'el_as', 'el_ba',
       'el_bi', 'el_ca', 'el_cl', 'el_fe', 'el_fe2', 'el_ni',
       'el_pb', 'el_sb']


fecha_ini='2019-06-15'


#end_date = date_1 + datetime.timedelta(days=10)
fecha_iteracion = pd.to_datetime(fecha_ini,dayfirst=True,utc=True)

Vector_Seccion_Activa=np.ones(10, dtype=bool)

#Vector_Seccion_Activa[6]=False

numero_ch=14
n=7
for i in range (3):
     
    DIAS,PREDICCION_CH,PREDICCION_SECC=Run_models(Vector_Seccion_Activa,Data_Circuito,fecha_iteracion,numero_ch,Data_Seccion,net,outputs,x,net_eficiencia,outputs_eficiencia,xef,net_comp_quim,outputs_comp_quim,xcq,net_rechazo,outputs_rechazo,xrech,Max_min_output_cosecha,Max_min_output_circuito,Max_min_input_cosecha,Max_min_input_circuito)
    
    #print(PREDICCION_CH)
    #print(PREDICCION_SECC)
    print(DIAS)
    #print(Vector_Seccion_Activa)
    #print(PREDICCION_CH)
    
 
    
     #transformo index a index de 1 a 10
    newindex=[0]*len(DIAS)
    
    for j in (range(len(DIAS))):
        
        newindex[j]=extractDigit(DIAS.index[j],100)
    
    DIAS = DIAS.reset_index(drop=True)
    DIAS['new_col'] = newindex
    DIAS.set_index('new_col')
    new_index = np.arange(1,11,1)
    DIAS=DIAS.set_index('new_col').reindex(new_index, fill_value='NaN')
    #print(DIAS)
    #print(Vector_Seccion_Activa)
    for k in range(len(DIAS)):
        DIAS['Dias Activa'].values
        if ((float(DIAS['Dias Activa'][k+1]) >= n ) or DIAS['Dias Activa'][k+1]=='NaN'):
            Vector_Seccion_Activa[k]=False
            
    #sumamos un día
    #result = Data_Circuito.iloc[:2].append(Data_Circuito, sort=False)
    fecha_iteracion = pd.to_datetime(fecha_iteracion, dayfirst=True, utc=True)+ timedelta(days=1)
    
    f=str(fecha_iteracion)
    b="00:00:00+00:00"
    f = f.replace(b,"03:00:00+0000")
  
    #print (Data_Circuito)
    Data_Circuito=Data_Circuito.reset_index(drop=False)
    Data_Circuito = Data_Circuito.set_index('fecha') 
    
    
    Data_Circuito.loc[f,['el_as','el_bi', 'el_sb']]=PREDICCION_CH.loc[14,'el_as'],PREDICCION_CH.loc[14,'el_bi'],PREDICCION_CH.loc[14,'el_sb']
    Data_Circuito=Data_Circuito.reset_index(drop=False)
    Data_Circuito = Data_Circuito.set_index('circuito',drop=True)
    
    #print(Data_Circuito.columns)
            
    
    
        
          
            
                 
    


# In[69]:




